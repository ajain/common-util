<?php
/**
 * Created by PhpStorm.
 * User: qj
 * Date: 2020/5/28
 * Time: 上午9:19
 */

namespace Util;

/**
 * 时间处理
 * @package Util
 */
class TimeUtil
{
    /**
     * 返回格式化时间
     *
     * @param string $time
     * @param string $format
     *
     * @return false|string
     */
    public static function timeFormat($time = '', $format = '')
    {
        // 默认时间格式
        if (empty($format)) {
            $format = 'Y-m-d H:i:s';
        }
        if (empty($time)) {
            $time = time();
        }
        $format_time = date($format, $time);
        return $format_time;
    }


    /**
     * 日期时间显示格式转换
     *
     * @param int $time 时间戳
     *
     * @return bool|string
     */
    public static function transferShowTime($time = 0)
    {
        // 时间显示格式
        $dayTime = date("m-d H:i", $time);
        $hourTime = date("H:i", $time);
        // 时间差
        $diffTime = time() - $time;
        $date = $dayTime;
        if ($diffTime < 60) {
            $date = '刚刚';
        } else if ($diffTime < 60 * 60) {
            $min = floor($diffTime / 60);
            $date = $min . '分钟前';
        } else if ($diffTime < 60 * 60 * 24) {
            $h = floor($diffTime / (60 * 60));
            $date = $h . '小时前 ' . $hourTime;
        } else if ($diffTime < 60 * 60 * 24 * 3) {
            $day = floor($diffTime / (60 * 60 * 24));
            if ($day == 1) {
                $date = '昨天 ' . $diffTime;
            } else {
                $date = '前天 ' . $diffTime;
            }
        }
        return $date;
    }


    /**
     * 获取毫秒数
     *
     * @return string
     */
    public static function getMillisecond()
    {
        list($t1, $t2) = explode(' ', microtime());
        $ms = sprintf('%.0f', (floatval($t1) + floatval($t2)) * 1000);
        return $ms;
    }

    /**
     * 校验是否为合法格式的时间
     *
     * @param string $time 时分秒时间
     * @param string $sep  分隔符，默认为冒号:
     *
     * @return bool
     */
    public static function checkTime($time = '', $sep = ":")
    {
        $timeArr = explode($sep, $time);
        $res = false;
        // 校验时间的时分秒是否在合理范围内
        if (count($timeArr) == 3 && is_numeric($timeArr[0]) && is_numeric($timeArr[1]) && is_numeric($timeArr[2])) {
            if (($timeArr[0] >= 0 && $timeArr[0] <= 23) && ($timeArr[1] >= 0 && $timeArr[1] <= 59) && ($timeArr[2] >= 0 && $timeArr[2] <= 59)) {
                $res = true;
            }
        }
        return $res;
    }

    /**
     * 获取两个标准时间格式的时间差（Y-m-d H:i:s）
     *
     * @param string $startTime 开始时间
     * @param string $endTime   结束时间
     *
     * @return array
     */
    public static function getDiffTimeData($startTime = '', $endTime = '')
    {
        $data = ['diff' => 0, 'day' => 0, 'hour' => 0, 'minute' => 0, 'second' => 0];
        if (empty($startTime) || empty($endTime)) {
            return $data;
        }
        // 开始时间的时间戳
        $startTime = strtotime($startTime);
        // 结束时间的时间戳
        $endTime = strtotime($endTime);
        // 得出时间戳差值
        $diff = $endTime - $startTime;
        if ($diff < 0) {
            return $data;
        }
        // 舍去法取整，获取天数
        $day = floor($diff / 3600 / 24);
        // 小时数
        $hour = floor(($diff % (3600 * 24)) / 3600);
        // 分钟
        $minute = floor(($diff % (3600 * 24)) % 3600 / 60);
        // 秒数
        $second = floor(($diff % (3600 * 24)) % 60);
        $data['diff'] = $diff;
        $data['day'] = $day;
        $data['hour'] = $hour;
        $data['minute'] = $minute;
        $data['second'] = $second;
        return $data;
    }

    /**
     * 手动创建一个唯一的UUID
     *
     * @return string
     */
    public static function createUuid()
    {
        // 根据当前时间（微秒计）生成唯一ID
        mt_srand((double)microtime() * 10000);
        $uuid = strtolower(md5(uniqid(rand(), true)));
        return $uuid;
    }

}