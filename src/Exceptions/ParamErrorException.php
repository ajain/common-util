<?php
/**
 * Created by PhpStorm.
 * User: qj
 * Date: 2020/5/28
 * Time: 上午9:40
 */

namespace Util\Exceptions;

use Exception;

class ParamErrorException extends Exception
{
    public function getName()
    {
        return 'ParamErrorException';
    }
}