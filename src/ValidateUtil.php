<?php
/**
 * Created by PhpStorm.
 * User: qj
 * Date: 2020/5/28
 * Time: 上午10:25
 */

namespace Util;

class ValidateUtil
{
    /**
     * 校验是否为合法格式的手机号
     * @param string $mobile 手机号码
     * @return bool
     */
    public static function checkMobile($mobile = '')
    {
        // 非数字直接false
        if (!is_numeric($mobile)) {
            return false;
        }
        $pattern = '/^13[\d]{9}$|^14[5,7]{1}\d{8}$|^15[^4]{1}\d{8}$|^17[0,6,3,7,8]{1}\d{8}$|^18[\d]{9}$|^19[9]{1}\d{8}$/';
        $res = preg_match($pattern, $mobile) ? true : false;
        return $res;
    }

    /**
     * 校验是否为合法格式的电话号码
     * @param string $telephone 电话号码
     * @return bool
     */
    public static function checkTelephone($telephone = '')
    {
        $pattern = '/^(0[0-9]{2,3})?[-]?\d{7,8}$/';
        $res = preg_match($pattern, $telephone) ? true : false;
        return $res;
    }

    /**
     * 校验是否为合法格式的邮箱
     * @param string $email 邮箱
     * @return bool
     */
    public static function checkEmail($email = '')
    {
        $pattern = '/([\w\-]+\@[\w\-]+\.[\w\-]+)/';
        $res = preg_match($pattern, $email) ? true : false;
        return $res;
    }

    /**
     *  校验是否为合法的IP地址
     * @param string $ip IP地址
     * @return bool
     */
    public static function checkIp($ip = '')
    {
        $pattern = '/^(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9])\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[0-9])$/';
        $res = preg_match($pattern, $ip) ? true : false;
        return $res;
    }

    /**
     * 校验指定范围长度的字符串名称
     * @param string $name 名称
     * @param int $min 最小长度
     * @param int $max 最大长度
     * @param string $char 字符串类型：EN英文，CN中文，ALL全部字符
     * @return bool
     */
    public static function checkName($name = '',$min = 2, $max = 20,  $char = 'ALL')
    {
        switch ($char) {
            case 'EN':
                $pattern = '/^[a-zA-Z]{' . $min . ',' . $max . '}$/iu';
                break;
            case 'CN':
                $pattern = '/^[_\x{4e00}-\x{9fa5}]{' . $min . ',' . $max . '}$/iu';
                break;
            default:
                $pattern = '/^[_\w\d\x{4e00}-\x{9fa5}]{' . $min . ',' . $max . '}$/iu';
        }
        $res = preg_match($pattern, $name) ? true : false;
        return $res;
    }

    /**
     * 校验是否为合法的邮政编码
     * @param string $code 邮政编码
     * @return bool
     */
    public static function checkPostCode($code = '')
    {
        $pattern = '/\d{6}/';
        $res = preg_match($pattern, $code) ? true : false;
        return $res;
    }

    /**
     * 校验是否为合法格式的日期
     * @param string $date 日期
     * @param string $sep 分隔符，默认为横线-
     * @return bool
     */
    public static function checkDate($date = '', $sep = '-')
    {
        $date_arr = explode($sep, $date);
        $res = false;
        // 校验日期是否为合法数字
        if (count($date_arr) == 3 && is_numeric($date_arr[0]) && is_numeric($date_arr[1]) && is_numeric($date_arr[2])) {
            $res = checkdate($date_arr[1], $date_arr[2], $date_arr[0]);
        }
        return $res;
    }

    /**
     * 校验是否为合法格式的时间
     * @param string $time 时分秒时间
     * @param string $sep 分隔符，默认为冒号:
     * @return bool
     */
    public static function checkTime($time = '', $sep = ":")
    {
        $time_arr = explode($sep, $time);
        $res = false;
        // 校验时间的时分秒是否在合理范围内
        if (count($time_arr) == 3 && is_numeric($time_arr[0]) && is_numeric($time_arr[1]) && is_numeric($time_arr[2])) {
            if (($time_arr[0] >= 0 && $time_arr[0] <= 23) && ($time_arr[1] >= 0 && $time_arr[1] <= 59) && ($time_arr[2] >= 0 && $time_arr[2] <= 59)) {
                $res = true;
            }
        }
        return $res;
    }


}