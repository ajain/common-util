<?php
/**
 * Created by PhpStorm.
 * User: qj
 * Date: 2020/5/28
 * Time: 上午10:05
 */

namespace Util;

class CommonUtil
{
    /**
     * 获取指定长度的随机字符串
     * @param int $length 随机字符串的长度
     * @return string
     */
    public static function getRandomStr($length = 9)
    {
        // 字符串集合，全部大小写字母和数字
        $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        $str = '';
        // 循环多次，随机逐次获取
        for ($i = 0; $i < $length; $i++) {
            $str .= $chars[mt_rand(0, strlen($chars) - 1)];
        }
        return $str;
    }


    /**
     * 统计文章中文字和图片的数量
     * @param string $html 文章html字符串
     * @return array
     */
    public static function countWordImg($html = '')
    {
        // 匹配img标签的数量
        preg_match_all('/<img /i', $html, $match_arr);
        $img_count = count($match_arr[0]);
        // 统计非img标签的数量，可根据实际情况进行调整表达式
        $pattern = '/\[#img_[0-9]+_[a-z]*_[0-9]+_[a-zA-Z]*/i';
        preg_match_all($pattern, $html, $match_arr);
        $img_count += count($match_arr[0]);
        // 去掉图片img标签
        $html = preg_replace("/<img([^>].+)>/iU", "", $html);
        // 去掉非标签的图片
        $html = preg_replace($pattern, "", $html);
        // 去掉全部空格
        $html = str_replace(' ', '', $html);
        // 先去除HTML和PHP标记，再统计字数
        $word_count = mb_strlen(trim(strip_tags($html)), 'UTF-8');
        return ['word_count' => $word_count, 'img_count' => $img_count];
    }

    /**
     * 格式化打印输出内容
     * @param void $data 需要打印的内容
     * @param bool $exit
     */
    public static function dump($data, $exit = true)
    {
        // 自定义样式，方便美观查看
        $output = '<pre style="display: block;background-color: #f5f5f5;border: 1px solid #cccccc;padding: 10px;margin: 45px 0 0 0;font-size: 13px;line-height: 1.5;border-radius: 4px;">';
        // boolean或者null类型直接文字输出，其他print_r格式化输出
        if (is_bool($data)) {
            $show = $data ? 'true' : 'false';
        } else if (is_null($data)) {
            $show = 'null';
        } else {
            $show = var_export($data, true);
        }
        // 拼接文本输出
        $output .= $show;
        $output .= '</pre>';
        echo $output;
        // 是否中断执行
        if ($exit) {
            exit();
        }
    }

    /**
     * 通过IP地址获取位置信息
     * @param string $ip IP地址
     * @return mixed
     */
    public static function getIpInfo($ip = '')
    {
        $api = 'http://www.geoplugin.net/json.gp?ip=';
        $res_data = file_get_contents($api . $ip);
        $res = json_decode($res_data, true);
        $data = [];
        if ($res['geoplugin_status'] == '200') {
            // 国家
            $data['country'] = $res['geoplugin_countryName'];
            // 省份
            $data['province'] = $res['geoplugin_regionName'];
            // 城市
            $data['city'] = $res['geoplugin_city'];
            // 经度
            $data['latitude'] = $res['geoplugin_latitude'];
            // 纬度
            $data['longitude'] = $res['geoplugin_longitude'];
            // 其他值，根据需要自定义
        }
        return $data;
    }

    /**
     * 发红包，金额随机
     * @param int $total 红包金额
     * @param int $num 红包个数
     * @param float $min 红包最小金额
     * @@return array
     */
    public static function getRedPacket($total = 0, $num = 1, $min = 0.01)
    {
        $data = [];
        for ($i = 1; $i < $num; $i++) {
            // 随机金额安全上限控制
            $safe_total = ($total - ($num - $i) * $min) / ($num - $i);
            $money = mt_rand($min * 100, $safe_total * 100) / 100;
            $total -= $money;
            // sort为领取顺序，money为红包金额，balance为领取后的余额
            $data[] = [
                'sort' => $i,
                'money' => $money,
                'balance' => $total
            ];
        }
        // 最后一个红包
        $data[] = [
            'sort' => $num,
            'money' => $total,
            'balance' => 0
        ];
        return $data;
    }

    /**
     * 获取文字的首字母
     * @param string $str 文字字符串
     * @return string
     */
    public static function getFirstChar($str = '')
    {
        $firstChar = $str[0];
        // 判断是否为字符串
        if (ord($firstChar) >= ord("A") && ord($firstChar) <= ord("z")) {
            return strtoupper($firstChar);
        }
        $str = iconv("UTF-8", "gb2312", $str);
        $asc = ord($str[0]) * 256 + ord($str[1]) - 65536;
        $firstChar = '';
        if ($asc >= -20319 && $asc <= -20284) $firstChar = "A";
        else if ($asc >= -20283 && $asc <= -19776) $firstChar = "B";
        else if ($asc >= -19775 && $asc <= -19219) $firstChar = "C";
        else if ($asc >= -19218 && $asc <= -18711) $firstChar = "D";
        else if ($asc >= -18710 && $asc <= -18527) $firstChar = "E";
        else if ($asc >= -18526 && $asc <= -18240) $firstChar = "F";
        else if ($asc >= -18239 && $asc <= -17923) $firstChar = "G";
        else if ($asc >= -17922 && $asc <= -17418) $firstChar = "H";
        else if ($asc >= -17417 && $asc <= -16475) $firstChar = "J";
        else if ($asc >= -16474 && $asc <= -16213) $firstChar = "K";
        else if ($asc >= -16212 && $asc <= -15641) $firstChar = "L";
        else if ($asc >= -15640 && $asc <= -15166) $firstChar = "M";
        else if ($asc >= -15165 && $asc <= -14923) $firstChar = "N";
        else if ($asc >= -14922 && $asc <= -14915) $firstChar = "O";
        else if ($asc >= -14914 && $asc <= -14631) $firstChar = "P";
        else if ($asc >= -14630 && $asc <= -14150) $firstChar = "Q";
        else if ($asc >= -14149 && $asc <= -14091) $firstChar = "R";
        else if ($asc >= -14090 && $asc <= -13319) $firstChar = "S";
        else if ($asc >= -13318 && $asc <= -12839) $firstChar = "T";
        else if ($asc >= -12838 && $asc <= -12557) $firstChar = "W";
        else if ($asc >= -12556 && $asc <= -11848) $firstChar = "X";
        else if ($asc >= -11847 && $asc <= -11056) $firstChar = "Y";
        else if ($asc >= -11055 && $asc <= -10247) $firstChar = "Z";
        return $firstChar;
    }

    /**
     * 删除指定目录下的文件夹和文件
     * @param string $path 目录路径
     */
    public static function deleteDir($path = '')
    {
        // 为空默认当前目录
        if ($path == '') {
            $path = realpath('.');
        }
        // 判断目录是否存在
        if (!is_dir($path)) {
            exit('目录【' . $path . '】不存在');
        }
        // 删除path目录最后的/
        if (substr($path, -1, 1) == '/') {
            $path = substr_replace($path, '', -1, 1);
        }
        // 扫描一个文件夹内的所有文件夹和文件
        $fileArr = scandir($path);
        foreach ($fileArr as $file) {
            // 排除当前目录.与父级目录..
            if ($file != "." && $file != "..") {
                // 如果是目录则递归子目录
                $thisFile = $path . DIRECTORY_SEPARATOR . $file;
                if (is_dir($thisFile)) {
                    // 继续循环遍历子目录
                    self::deleteDir($thisFile);
                    // 删除空文件夹
                    @rmdir($thisFile);
                } else if (is_file($thisFile)) {
                    // 文件类型直接删除
                    unlink($thisFile);
                }
            }
        }
    }

    /**
     * 返回格式化数字
     * @param int $number 待格式化数字
     * @param int $decimals 保留小数位数，默认2位
     * @param string $decPoint 整数和小数分隔符号
     * @param string $thousandsSep 整数部分每三位数读分隔符号
     * @return string
     */
    public static function numberFormatPlus($number = 0, $decimals = 2, $decPoint = '.', $thousandsSep = ',')
    {
        $format_num = '0.00';
        if (is_numeric($number)) {
            $format_num = number_format($number, $decimals, $decPoint, $thousandsSep);
        }
        return $format_num;
    }

    /**
     * 人民币数字小写转大写
     * @param int $money 人民币数值
     * @param string $intUnit 币种单位，默认"元"，有的需求可能为"圆"
     * @param bool $isRound 是否对小数进行四舍五入
     * @param bool $isExtraZero 是否对整数部分以0结尾，小数存在的数字附加0,比如1960.30
     * @return string
     */
    public static function rmbFormat($money = 0, $intUnit = '元', $isRound = true, $isExtraZero = false)
    {
        // 非数字，原样返回
        if (!is_numeric($money)) {
            return $money;
        }
        // 将数字切分成两段
        $parts = explode('.', $money, 2);
        $int = isset($parts[0]) ? strval($parts[0]) : '0';
        $dec = isset($parts[1]) ? strval($parts[1]) : '';
        // 如果小数点后多于2位，不四舍五入就直接截，否则就处理
        $dec_len = strlen($dec);
        if (isset($parts[1]) && $dec_len > 2) {
            $dec = $isRound ? substr(strrchr(strval(round(floatval("0." . $dec), 2)), '.'), 1) : substr($parts [1], 0, 2);
        }
        // 当number为0.001时，小数点后的金额为0元
        if (empty($int) && empty($dec)) {
            return '零';
        }
        // 定义
        $chs = ['0', '壹', '贰', '叁', '肆', '伍', '陆', '柒', '捌', '玖'];
        $uni = ['', '拾', '佰', '仟'];
        $dec_uni = ['角', '分'];
        $exp = ['', '万'];
        $res = '';
        // 整数部分从右向左找
        for ($i = strlen($int) - 1, $k = 0; $i >= 0; $k++) {
            $str = '';
            // 按照中文读写习惯，每4个字为一段进行转化，i一直在减
            for ($j = 0; $j < 4 && $i >= 0; $j++, $i--) {
                // 非0的数字后面添加单位
                $u = $int[$i] > 0 ? $uni[$j] : '';
                $str = $chs[$int[$i]] . $u . $str;
            }
            // 去掉末尾的0
            $str = rtrim($str, '0');
            // 替换多个连续的0
            $str = preg_replace("/0+/", "零", $str);
            if (!isset($exp[$k])) {
                // 构建单位
                $exp[$k] = $exp[$k - 2] . '亿';
            }
            $u2 = $str != '' ? $exp[$k] : '';
            $res = $str . $u2 . $res;
        }
        // 如果小数部分处理完之后是00，需要处理下
        $dec = rtrim($dec, '0');
        // 小数部分从左向右找
        if (!empty($dec)) {
            $res .= $intUnit;
            // 是否要在整数部分以0结尾的数字后附加0，有的系统有这要求
            if ($isExtraZero) {
                if (substr($int, -1) === '0') {
                    $res .= '零';
                }
            }
            for ($i = 0, $cnt = strlen($dec); $i < $cnt; $i++) {
                // 非0的数字后面添加单位
                $u = $dec[$i] > 0 ? $dec_uni[$i] : '';
                $res .= $chs[$dec[$i]] . $u;
                if ($cnt == 1)
                    $res .= '整';
            }
            // 去掉末尾的0
            $res = rtrim($res, '0');
            // 替换多个连续的0
            $res = preg_replace("/0+/", "零", $res);
        } else {
            $res .= $intUnit . '整';
        }
        return $res;
    }

    /**
     * 导出excel表格数据
     * @param array $data 表格数据，一个二维数组
     * @param array $title 第一行标题，一维数组
     * @param string $filename 下载的文件名
     */
    public static function exportExcel($data = [], $title = [], $filename = '')
    {
        // 默认文件名为时间戳
        if (empty($filename)) {
            $filename = time();
        }
        // 定义输出header信息
        header("Content-type:application/octet-stream;charset=GBK");
        header("Accept-Ranges:bytes");
        header("Content-type:application/vnd.ms-excel");
        header("Content-Disposition:attachment;filename=" . $filename . ".xls");
        header("Pragma: no-cache");
        header("Expires: 0");
        ob_start();
        echo "<head><meta http-equiv='Content-type' content='text/html;charset=GBK' /></head> <table border=1 style='text-align:center'>\n";
        // 导出xls开始，先写表头
        if (!empty($title)) {
            foreach ($title as $k => $v) {
                $title[$k] = iconv("UTF-8", "GBK//IGNORE", $v);
            }
            $title = "<td>" . implode("</td>\t<td>", $title) . "</td>";
            echo "<tr>$title</tr>\n";
        }
        // 再写表数据
        if (!empty($data)) {
            foreach ($data as $key => $val) {
                foreach ($val as $ck => $cv) {
                    if (is_numeric($cv) && strlen($cv) < 12) {
                        $data[$key][$ck] = '<td>' . mb_convert_encoding($cv, "GBK", "UTF-8") . "</td>";
                    } else {
                        $data[$key][$ck] = '<td style="vnd.ms-excel.numberformat:@;">' . iconv("UTF-8", "GBK//IGNORE", $cv) . "</td>";
                    }
                }
                $data[$key] = "<tr>" . implode("\t", $data[$key]) . "</tr>";
            }
            echo implode("\n", $data);
        }
        echo "</table>";
        ob_flush();
        exit;
    }


    /**
     * 支持断点续传，下载文件
     * @param string $file 下载文件完整路径
     */
    public static function downloadFileResume($file)
    {
        // 检测文件是否存在
        if (!is_file($file)) {
            die("非法文件下载！");
        }
        // 打开文件
        $fp = fopen("$file", "rb");
        // 获取文件大小
        $size = filesize($file);
        // 获取文件名称
        $filename = basename($file);
        // 获取文件扩展名
        $file_extension = strtolower(substr(strrchr($filename, "."), 1));
        // 根据扩展名 指出输出浏览器格式
        switch ($file_extension) {
            case "exe":
                $ctype = "application/octet-stream";
                break;
            case "zip":
                $ctype = "application/zip";
                break;
            case "mp3":
                $ctype = "audio/mpeg";
                break;
            case "mpg":
                $ctype = "video/mpeg";
                break;
            case "avi":
                $ctype = "video/x-msvideo";
                break;
            default:
                $ctype = "application/force-download";
        }
        // 通用header头信息
        header("Cache-Control:");
        header("Cache-Control: public");
        header("Content-Type: $ctype");
        header("Content-Disposition: attachment; filename=$filename");
        header("Accept-Ranges: bytes");
        // 如果有$_SERVER['HTTP_RANGE']参数
        if (isset($_SERVER['HTTP_RANGE'])) {
            // 断点后再次连接$_SERVER['HTTP_RANGE']的值
            list($a, $range) = explode("=", $_SERVER['HTTP_RANGE']);
            str_replace($range, "-", $range);
            // 文件总字节数
            $size2 = $size - 1;
            // 获取下次下载的长度
            $new_length = $size2 - $range;
            header("HTTP/1.1 206 Partial Content");
            // 输入总长
            header("Content-Length: $new_length");
            header("Content-Range: bytes $range$size2/$size");
            // 设置指针位置
            fseek($fp, $range);
        } else {
            // 第一次连接下载
            $size2 = $size - 1;
            header("Content-Range: bytes 0-$size2/$size");
            // 输出总长
            header("Content-Length: " . $size);
        }
        // 虚幻输出
        while (!feof($fp)) {
            // 设置文件最长执行时间
            set_time_limit(0);
            // 输出文件
            print(fread($fp, 1024 * 8));
            // 输出缓冲
            flush();
            ob_flush();
        }
        fclose($fp);
        exit;
    }

    /**
     * 获取用户真实的IP地址
     * @return mixed
     */
    public static function getRealIp()
    {
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }

    /**
     * 获取短网址链接
     * @param string $url 长网址
     * @return string
     */
    public static function getShortUrl($url = '')
    {
        // 直接请求第三方接口地址，获取短URL
        $apiUrl = 'http://tinyurl.com/api-create.php?url=';
        $shortUrl = file_get_contents($apiUrl . $url);
        return $shortUrl;
    }


}