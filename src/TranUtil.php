<?php
/**
 * Created by PhpStorm.
 * User: qj
 * Date: 2020/5/28
 * Time: 上午10:22
 */

namespace Util;

class TranUtil
{
    /**
     * 将xml格式转换为数组
     * @param string $xml xml字符串
     * @return mixed
     */
    public static function xmlToArray($xml = '')
    {
        // 利用函数simplexml_load_string()把xml字符串载入对象中
        $obj = simplexml_load_string($xml, 'SimpleXMLElement', LIBXML_NOCDATA);
        // 编码对象后，再解码即可得到数组
        $arr = json_decode(json_encode($obj), true);
        return $arr;
    }

    /**
     * 隐藏手机号中间四位数为****
     * @param string $mobile 正常手机号
     * @return mixed
     */
    public static function replacePhone($mobile = '')
    {
        $new_mobile = substr_replace($mobile, '****', 3, 4);
        return $new_mobile;
    }

    /**
     * 最简单的Ajax请求返回数据格式
     * @param string $msg 返回提示信息
     * @param int $code 返回标识符号
     * @param array $data 返回数据
     */
    public static function ajaxReturn($msg = '', $code = 0, $data = [])
    {
        $return['code'] = $code;
        $return['msg'] = $msg;
        $return['data'] = $data;
        exit(json_encode($return, JSON_UNESCAPED_UNICODE));
    }

    /**
     * 截取字符串，超出部分用省略符号显示
     * @param string $text 待截取字符串
     * @param int $length 截取长度，默认全部截取
     * @param string $rep 截取超出替换的字符串，默认为省略号
     * @return string
     */
    public static function cutString($text = '', $length = 0, $rep = '…')
    {
        if (!empty($length) && mb_strlen($text, 'utf8') > $length) {
            $text = mb_substr($text, 0, $length, 'utf8') . $rep;
        }
        return $text;
    }

    /**
     * CURL请求之GET方式
     * @param string $url 请求接口地址
     * @return bool|mixed
     */
    public static function curlGet($url = '')
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        // 不验证SSL证书。
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $res = curl_exec($ch);
        curl_close($ch);
        return $res;
    }

    /**
     * CURL请求之POST方式
     * @param string $url 请求接口地址
     * @param array $data 请求参数
     * @param int $timeout 超时时间
     * @return mixed
     */
    public static function curlPost($url = '', $data = [], $timeout = 3000)
    {
        $post_data = http_build_query($data, '', '&');
        header("Content-type:text/html;charset=utf-8");
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        curl_setopt($ch, CURLOPT_HEADER, false);
        $res = curl_exec($ch);
        curl_close($ch);
        return $res;
    }

    /**
     * XML编码
     * @param mixed $data 数据
     * @param string $encoding 数据编码
     * @param string $root 根节点名
     * @return string
     */
    public static function xmlEncode($data, $encoding='utf-8', $root='root') {
        $xml    = '<?xml version="1.0" encoding="' . $encoding . '"?>';
        $xml   .= '<' . $root . '>';
        $xml   .= self::dataToXml($data);
        $xml   .= '</' . $root . '>';
        return $xml;
    }

    /**
     * 数据XML编码
     * @param mixed $data 数据
     * @return string
     */
    public static function dataToXml($data) {
        $xml = '';
        foreach ($data as $key => $val) {
            is_numeric($key) && $key = "item id=\"$key\"";
            $xml    .=  "<$key>";
            $xml    .=  ( is_array($val) || is_object($val)) ? self::dataToXml($val) : $val;
            list($key, ) = explode(' ', $key);
            $xml    .=  "</$key>";
        }
        return $xml;
    }

}