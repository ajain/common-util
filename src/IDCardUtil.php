<?php
/**
 * Created by PhpStorm.
 * User: qj
 * Date: 2020/5/28
 * Time: 上午9:35
 */

namespace Util;
use Util\Exceptions\ParamErrorException;

/**
 * 身份证相关处理
 *
 * @package Util
 */
class IDCardUtil
{
    /**
     * 校验是否为合法的身份证号
     *
     * @param string $idCard 身份证号
     *
     * @return bool
     */
    public static function checkIdCard($idCard = '')
    {
        $pattern = '/^\d{6}((1[89])|(2\d))\d{2}((0\d)|(1[0-2]))((3[01])|([0-2]\d))\d{3}(\d|X)$/i';
        $res = preg_match($pattern, $idCard) ? true : false;
        return $res;
    }

    /**
     * 通过身份证号计算年龄
     *
     * @param string $idCard 身份证号
     *
     * @return int
     */
    function getAgeByIdCard($idCard = '')
    {
        // 获取出生年月日
        $year = substr($idCard, 6, 4);
        $month = substr($idCard, 10, 2);
        $day = substr($idCard, 12, 2);
        // 计算时间年月日差
        $age = date("Y", time()) - $year;
        $diffMonth = date("m") - $month;
        $diffDay = date("d") - $day;
        // 不满周岁年龄减1
        if ($age < 0 || $diffMonth < 0 || $diffDay < 0) {
            $age--;
        }
        return $age;
    }


    /**
     * 通过身份证获取所属星座
     *
     * @param string $idCard
     *
     * @return string
     * @throws \Util\Exceptions\ParamErrorException
     */
    public static function getConstellationByCard($idCard = '')
    {
        if(!self::checkIdCard($idCard)) {
            throw new ParamErrorException('身份证不合法');
        }
        // 截取生日的时间
        $birthday = substr($idCard, 10, 4);
        $month = substr($birthday, 0, 2);
        $day = substr($birthday, 2);
        // 判断时间范围获取星座
        $constellation = "";
        if (($month == 1 && $day >= 21) || ($month == 2 && $day <= 19)) $constellation = "水瓶座";
        else if (($month == 2 && $day >= 20) || ($month == 3 && $day <= 20)) $constellation = "双鱼座";
        else if (($month == 3 && $day >= 21) || ($month == 4 && $day <= 20)) $constellation = "白羊座";
        else if (($month == 4 && $day >= 21) || ($month == 5 && $day <= 21)) $constellation = "金牛座";
        else if (($month == 5 && $day >= 22) || ($month == 6 && $day <= 21)) $constellation = "双子座";
        else if (($month == 6 && $day >= 22) || ($month == 7 && $day <= 22)) $constellation = "巨蟹座";
        else if (($month == 7 && $day >= 23) || ($month == 8 && $day <= 23)) $constellation = "狮子座";
        else if (($month == 8 && $day >= 24) || ($month == 9 && $day <= 23)) $constellation = "处女座";
        else if (($month == 9 && $day >= 24) || ($month == 10 && $day <= 23)) $constellation = "天秤座";
        else if (($month == 10 && $day >= 24) || ($month == 11 && $day <= 22)) $constellation = "天蝎座";
        else if (($month == 11 && $day >= 23) || ($month == 12 && $day <= 21)) $constellation = "射手座";
        else if (($month == 12 && $day >= 22) || ($month == 1 && $day <= 20)) $constellation = "魔羯座";
        return $constellation;
    }

    /**
     * 通过身份证获取所属生肖
     *
     * @param string $idCard
     *
     * @return string
     * @throws \Util\Exceptions\ParamErrorException
     */
    public static function getAnimalByCard($idCard = '')
    {
        if(!self::checkIdCard($idCard)) {
            throw new ParamErrorException('身份证不合法');
        }
        // 获取出生年份
        $start = 1901;
        $end = substr($idCard, 6, 4);
        $remainder = ($start - $end) % 12;
        // 计算所属生肖
        $animal = "";
        if ($remainder == 1 || $remainder == -11) $animal = "鼠";
        else if ($remainder == 0) $animal = "牛";
        else if ($remainder == 11 || $remainder == -1) $animal = "虎";
        else if ($remainder == 10 || $remainder == -2) $animal = "兔";
        else if ($remainder == 9 || $remainder == -3) $animal = "龙";
        else if ($remainder == 8 || $remainder == -4) $animal = "蛇";
        else if ($remainder == 7 || $remainder == -5) $animal = "马";
        else if ($remainder == 6 || $remainder == -6) $animal = "羊";
        else if ($remainder == 5 || $remainder == -7) $animal = "猴";
        else if ($remainder == 4 || $remainder == -8) $animal = "鸡";
        else if ($remainder == 3 || $remainder == -9) $animal = "狗";
        else if ($remainder == 2 || $remainder == -10) $animal = "猪";
        return $animal;
    }

    /**
     * 通过身份证获取性别
     * @param string $idCard
     *
     * @return string
     * @throws \Util\Exceptions\ParamErrorException
     */
    public static function getSex($idCard = '')
    {
        if(!self::checkIdCard($idCard)) {
            throw new ParamErrorException('身份证不合法');
        }
        // 第十七位数字，偶数标识女性，奇数标识男性
        $sex_num = substr($idCard, 16, 1);
        $sex = $sex_num % 2 === 0 ? '女' : '男';
        return $sex;
    }

    /**
     * 通过身份证判断是否成年
     * @param string $idCard 身份证号
     * @return bool
     * @throws \Util\Exceptions\ParamErrorException
     */
    public static function checkAdult($idCard = '')
    {
        if(!self::checkIdCard($idCard)) {
            throw new ParamErrorException('身份证不合法');
        }
        // 获取出生年月日
        $year = substr($idCard, 6, 4);
        $month = substr($idCard, 10, 2);
        $day = substr($idCard, 12, 2);
        // 18年的秒数
        $adultTime = 18 * 365 * 24 * 60 * 60;
        // 出生年月日的时间戳
        $birthdayTime = mktime(0, 0, 0, $month, $day, $year);
        // 是否成年，默认未成年
        $isAdult = false;
        // 超过18岁则成年
        if ((time() - $birthdayTime) > $adultTime) {
            $isAdult = true;
        }
        return $isAdult;
    }

    /**
     * 通过身份证号获取省市区地址信息
     * @param string $id_card
     * @return array
     */
    public static function getAddressByIdCard($id_card = '')
    {
        // 获取政府代码前两位和前六位
        $twoCode = substr($id_card, 0, 2);
        $sixCode = substr($id_card, 0, 6);
        // 获取城市信息
        $province = Province::PROVINCE[$twoCode];
        $address = Area::AREA[$sixCode];
        $res = [
            'province' => $province,
            'area' => $address
        ];
        return $res;
    }
}